-- Author: Olivier Lenoir <olivier.len02@gmail.com>
-- Created: 2022-09-17 15:05:55
-- License: MIT, Copyright (c) 2022 Olivier Lenoir
-- Language: SQLite 3
-- Project: BE016
-- Description:

--  ====================================
--  Tables
--  ====================================
drop table if exists BE016;

create table if not exists BE016 (
    DMR_ID integer,
    QRZ_DX_ATTRIBUTE varchar(14),
    FIRST_NAME varchar(20),
    TOWN varchar(30),
    PROVINCE varchar(30),
    COUNTRY varchar(10),
    primary key (
        DMR_ID
    )
);


--  ====================================
--  Load data
--  ====================================
.mode csv

--.import '| tail -n +2 ../data/<datafile>.dsv' <table>
.import ../data/016-belgique.csv RAW_BE016


--  ====================================
--  Insert data
--  ====================================
insert into BE016
select
    *
from
    RAW_BE016
where
    "ID DMR" != ''
;


--  ====================================
--  Drop table
--  ====================================
drop table RAW_BE016;
