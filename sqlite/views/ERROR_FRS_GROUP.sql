-- Author: Olivier Lenoir <olivier.len02@gmail.com>
-- Created: 2022-09-24 09:52:46
-- License: MIT, Copyright (c) 2022 Olivier Lenoir
-- Language: SQLite 3
-- Project: ERROR_FRS_GROUP
-- Description:


drop view if exists ERROR_FRS_GROUP;

create view if not exists ERROR_FRS_GROUP as
select
    *,
    'Whitespace on both ends of QRZ_DX_ATTRIBUTE' ERROR
from
    FRS_GROUP
where
    QRZ_DX_ATTRIBUTE != trim(QRZ_DX_ATTRIBUTE)
union
select
    *,
    'Whitespace on both ends of FIRST_NAME' ERROR
from
    FRS_GROUP
where
    FIRST_NAME != trim(FIRST_NAME)
union
select
    *,
    'Whitespace on both ends of TOWN' ERROR
from
    FRS_GROUP
where
    TOWN != trim(TOWN)
union
select
    *,
    'Whitespace on both ends of PROVINCE' ERROR
from
    FRS_GROUP
where
    PROVINCE != trim(PROVINCE)
union
select
    *,
    'Whitespace on both ends of COUNTRY' ERROR
from
    FRS_GROUP
where
    COUNTRY != trim(COUNTRY)
union
select
    *,
    'Missing QRZ_DX_ATTRIBUTE' ERROR
from
    FRS_GROUP
where
    length(QRZ_DX_ATTRIBUTE) < 2
union
select
    *,
    'Missing FIRST_NAME' ERROR
from
    FRS_GROUP
where
    length(FIRST_NAME) < 2
union
select
    *,
    'Missing TOWN' ERROR
from
    FRS_GROUP
where
    length(TOWN) < 2
union
select
    *,
    'Missing PROVINCE' ERROR
from
    FRS_GROUP
where
    length(PROVINCE) < 2
    union
select
    *,
    'Missing COUNTRY' ERROR
from
    FRS_GROUP
where
    length(COUNTRY) < 2
;
