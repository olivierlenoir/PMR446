-- Author: Olivier Lenoir <olivier.len02@gmail.com>
-- Created: 2022-09-24 08:56:10
-- License: MIT, Copyright (c) 2022 Olivier Lenoir
-- Language: SQLite 3
-- Project: FRS_GROUP
-- Description:


drop view if exists FRS_GROUP;

create view if not exists FRS_GROUP as
select * from FR014
union all
select * from BE016
;
