-- Author: Olivier Lenoir <olivier.len02@gmail.com>
-- Created:
-- License: MIT, Copyright (c) <YYYY> Olivier Lenoir
-- Language: SQLite 3
-- Project:
-- Description:

.timer on
.header on


--  ====================================
--  Tables
--  ====================================
.read tables/FR014.sql
.read tables/BE016.sql


--  ====================================
--  Views
--  ====================================
.read views/FRS_GROUP.sql
.read views/ERROR_FRS_GROUP.sql


--  ====================================
--  Load data
--  ====================================



--  ====================================
--  vacuum
--  ====================================
vacuum;
