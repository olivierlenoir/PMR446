# Author: Olivier Lenoir <olivier.len02@gmail.com>
# Created: 2022-09-14 21:01:48
# Project: PMR446

TEXFILE=PMR446
DBFILE=PMR446

all: extract load

.PHONY: init extract doc cheatsheet cleandoc cleancheatsheet pull logo load

.SILENT: clean


init:
	mkdir -p data
	mkdir -p database
	mkdir -p sqlite
	mkdir -p img


extract:
	mkdir -p data
	rm -fr data/*.csv
	cd data ; wget http://groupe-frs.hamstation.eu/bdd/014-france.csv
	cd data ; wget http://groupe-frs.hamstation.eu/bdd/016-belgique.csv
	for CSV in data/*.csv ; do sed -i 1d $${CSV} ; done


load:
	mkdir -p database
#	rm -fr database/${DBFILE}.sqlite
	cd sqlite ; sqlite3 ../database/${DBFILE}.sqlite < Load.sql


logo:
	mkdir -p img
	convert -background darkkhaki -fill navy -gravity center -size 200x200 label:'PMR\n446\nMHz' img/PMR446.png


doc: cleandoc
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex
	make cleandoc


cheatsheet: cleancheatsheet
	pdflatex ${TEXFILE}CheatSheet.tex
	pdflatex ${TEXFILE}CheatSheet.tex
	pdflatex ${TEXFILE}CheatSheet.tex
	make cleancheatsheet


cleandoc:
	rm -f ${TEXFILE}.aux
	rm -f ${TEXFILE}.log
	rm -f ${TEXFILE}.out
	rm -f ${TEXFILE}.nav
	rm -f ${TEXFILE}.toc
	rm -f ${TEXFILE}.lof
	rm -f ${TEXFILE}.lot


cleancheatsheet:
	rm -f ${TEXFILE}CheatSheet.aux
	rm -f ${TEXFILE}CheatSheet.log
	rm -f ${TEXFILE}CheatSheet.out
	rm -f ${TEXFILE}CheatSheet.nav
	rm -f ${TEXFILE}CheatSheet.toc
	rm -f ${TEXFILE}CheatSheet.lof
	rm -f ${TEXFILE}CheatSheet.lot


pull:
	# Git pull
	git pull --all
